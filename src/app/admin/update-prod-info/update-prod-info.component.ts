import  Swal  from 'sweetalert2';
import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../core/httpservices/api.service';


@Component({
  selector: 'app-update-prod-info',
  templateUrl: './update-prod-info.component.html',
  styleUrls: ['./update-prod-info.component.scss']
})
export class UpdateProdInfoComponent implements OnInit {
@Input() type:any
@Input() p_type:any
@Input() format:any
@Input() mode:any

price_format:any
price_mode:any

spiner = false

  constructor(private api:ApiService) { }

  ngOnInit(): void {
    console.log(this.format)
    console.log(this.p_type)

  }

  Update_Format(item:any){
    let data:any={id:item.product_format_id , price: item.product_price_format }
    console.log(data)
    if(data.id && data.price){
      if(!this.spiner){
        this.spiner=!this.spiner
      }
      this.api.setProd_format(data.id, data.price).subscribe(res=>{
        //console.log(res)
        var resp = res.data
        console.log(resp)
        
        Swal.fire({
          icon:"success",
          text:"Votre mise à jour a été éffectué"
        
        },)
        this.spiner=!this.spiner;
        
      },err=>{
        console.log(err)
        this.spiner=!this.spiner;
        Swal.fire({
          icon:"error",
          text:"Une erreur s'est produite"
        })
        
        
        location.reload()
        
        
      })
    }else{
      Swal.fire(
        {
          icon:"warning",
          text:"une erreur s'est produite"
        }
      )    
    }

  }

  Update_Mode(item:any){
    let data:any={id: item.product_mode_id, price: item.product_mode_price}
    console.log(data)
    if(data.id && data.price){
      if(!this.spiner){
        this.spiner=!this.spiner
      }
      this.api.setProd_mode(data.id, data.price).subscribe(res=>{
        console.log(res)
        this.spiner=!this.spiner;
        Swal.fire({
          icon:"success",
          text:"Votre mise à jour a été éffectué"
        },
        )
      },err=>{
        
        this.spiner=!this.spiner;
        console.log(err);
        Swal.fire({
          icon:"error",
          text:"Une erreur s'est produite"
        })
        
      })
    }

  }

  update_PrintProd(item:any){
    var data:any={
      id: item.printing_id , 
      printing_price: item.printing_price, 
      color_price: item.color_price, 
      printing_discount: item.printing_discount

    }
    console.log(data)

    if(data.id && data.printing_price && data.color_price && data.printing_discount){
      if(!this.spiner){
        this.spiner=!this.spiner
      }
      this.api.setProd_printing(data.id, data.printing_price, data.color_price, data.printing_discount).subscribe(
        res=>{
          
          this.spiner=!this.spiner;
          Swal.fire({
            icon:"success",
            text:"Votre mise à jour a été effectué"
          },)
        },
        err=>{
          this.spiner=!this.spiner;
          console.log(err);
          Swal.fire({
            icon:"error",
            text:"une erreur s'est produite"
          })
        }
      )
    
    }else{
        Swal.fire(
          {
            icon:"warning",
            text:"renseigner tous les champs svp"
          }
        )    
    }
  }

  Update_TypeProd(item:any){

    var data:any={
      id: item.productype_id , discount: item.product_type_discount,
    }
    if(data.id && data.discount){
      if(!this.spiner){
        this.spiner=!this.spiner
      }
      this.api.setProd_type(data.id, data.discount).subscribe(
        res=>{
          var data:any=res.data
          //this.data = data
          this.spiner=!this.spiner;
          Swal.fire({
            icon:"success",
            text:"Votre mise à jour a été effectué"
          },)
        },
        err=>{
          this.spiner=!this.spiner;
          console.log(err);
          Swal.fire({
            icon:"error",
            text:"une erreur s'est produite"
          })
        }
      )
    
    }else{
        Swal.fire(
          {
            icon:"warning",
            text:"renseigner tous les champs svp"
          }
        )    
    }
  }

}
