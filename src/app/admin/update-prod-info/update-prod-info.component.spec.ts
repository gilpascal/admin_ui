import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProdInfoComponent } from './update-prod-info.component';

describe('UpdateProdInfoComponent', () => {
  let component: UpdateProdInfoComponent;
  let fixture: ComponentFixture<UpdateProdInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateProdInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProdInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
