import { Component, OnInit } from '@angular/core';
import {EventparserService ,AdminService, ApiService} from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-admindash',
  templateUrl: './admindash.component.html',
  styleUrls: ['./admindash.component.scss']
})
export class AdmindashComponent implements OnInit {
default:boolean=true;
create:boolean=false;
me:boolean=false
order:boolean=false
list_user:boolean=false
partner:boolean=false
aprv_partner:boolean=false
order_partner:boolean=false
partner_prod:boolean=false
update_pro:boolean=false;
addpro:boolean=false;
Orders:any=[]
menu:any={
  name:"me",
  user:"Dave",
  role:"software Engineer"
}
Categories:any=[]
add_livry=false
prod_format:any=[];
prod_mode:any=[];
prod_print:any=[];
Type:any=[];
users:any=[]
adress:Array<any>=[];
datalgth:Array<number>=[]
constructor(private EVENT:EventparserService, private http:ApiService,private tokenservice:TokenStorageServiceService) { }

  ngOnInit(): void {
    this.http.getUser().subscribe(res=>{
      var resp:any=res;
      this.users= resp.data;
      for(let item of this.users){
        this.users[this.users.indexOf(item)].role=JSON.parse(item.role)
      }

    });

    this.http.getdeliveryadr().subscribe(
      res=>{
        var resp:any=res
        this.adress=resp.data
        console.log(this.adress)
      },
      err=>{
        location.reload()
      }
    )
  this.http.getOrders(true).subscribe(res=>{
    let data:any=res
    var items:any=[]
    var pendingcpt=0;
    var newordercpt=0;
    var endecpt =0
    if(data.status){
      for(let item of data.data){
        item.url_dir = JSON.parse(item.url_dir);
        item.design_title= JSON.parse(item.design_title);
        item.url_dir[0] = environment.prodApiOrderBaseImage+item.url_dir[0]
        items.push(item)
        if(item.userorder_status.toLowerCase().trim()===("pending at production").trim()){
          pendingcpt=pendingcpt+1
        }

        if(item.userorder_status.toLowerCase().trim()===("new").trim()){
          newordercpt=newordercpt+1
        }

        if(item.userorder_status.toLowerCase().trim()===("production ended").trim()){
          endecpt= endecpt+1
        }

      }
      this.Orders=items
      console.log(this.Orders)
      this.datalgth.push(newordercpt)
      this.datalgth.push(pendingcpt)
      this.datalgth.push(endecpt)

    }
  },
  err=>{
  location.reload()
})






  this.http.getAllCategories().subscribe(res=>{
    var data:any= res
    if(data.status){
      this.Categories=data.data
      console.log(this.Categories)
    }
  },err=>{
    location.reload()
  })
  this.http.getProductType().subscribe(res=>{
    var data :any= res
    if(data.status){
      this.Type=data.data
      console.log(this.Type)
    }
  },err=>{
    location.reload()

  })

  this.http.getprod_format().subscribe(res=>{
    let resp:any = res
    if(resp.status){
      this.prod_format = res.data
      console.log(this.prod_format)
    }
  })

  this.http.getprod_mode().subscribe(res=>{
    let resp:any = res
    if(resp.status){
      this.prod_mode = res.data
      console.log(this.prod_mode)
    }
  })

  this.http.getprod_print().subscribe(res=>{
    let resp:any = res
    if(resp.status){
      this.prod_print = res.data
      console.log(this.prod_print)
    }
  })




    this.EVENT.update_pro.subscribe(
      menu=>{
        this.update_pro=menu;
        this.create=false;
        this.default=false;
        this.me=false
        this.list_user=false;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
        this.addpro=false;
        this.add_livry=false

      }
    )

    this.EVENT.add_pro.subscribe(
      menu=>{
       this.addpro=menu;
       this.update_pro=false;
       this.create=false;
       this.default=false;
       this.me=false
       this.list_user=false;
       this.order=false
       this.partner=false
       this.aprv_partner=false
       this.order_partner=false
       this.partner_prod=false
       this.add_livry=false
      }
    )
    this.EVENT.add_user.subscribe(
      menu=>{
        this.update_pro=false;
        this.addpro=false;
        this.create=menu;
        this.default=false;
        this.me=false
        this.list_user=false;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
        this.add_livry=false
      }
    )

    this.EVENT.new_orders.subscribe(
      menu=>{
        this.update_pro=false;
        this.addpro=false;
        this.default=false;
        this.create=false;
          this.me=false;
          this.list_user=false;
          this.order=menu
          this.partner=false
          this.aprv_partner=false
          this.order_partner=false
          this.partner_prod=false
          this.add_livry=false
      }
    )

    this.EVENT.profile.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=menu;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
        this.add_livry=false
    })

    this.EVENT.list_user.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=menu;
        this.me=false;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
        this.add_livry=false
    })
    this.EVENT.Partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=menu;
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
        this.add_livry=false
    })
    this.EVENT.Product_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=menu;
        this.add_livry=false
    })
    this.EVENT.Aprov_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=menu
        this.order_partner=false
        this.partner_prod=false;
        this.add_livry=false
    })
    this.EVENT.Order_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=false
        this.order_partner=menu;
        this.partner_prod=false;
        this.add_livry=false
    })
    this.EVENT.add_livry.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=false
        this.order_partner=false;
        this.partner_prod=false;
        this.add_livry=menu;
    })

    this.EVENT.logout.subscribe(msg=>{
      if(msg){
       this.tokenservice.signOut()
        setTimeout(()=>{
          location.href="/login"
        },1000)

      }
    })



  }


}
