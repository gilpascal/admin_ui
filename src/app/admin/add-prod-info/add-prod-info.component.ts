import { Component, OnInit,Input } from '@angular/core';
import { ApiService } from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
import  Swal  from 'sweetalert2'
import { EventparserService } from '../../core/communications/eventparser.service';
@Component({
  selector: 'app-add-prod-info',
  templateUrl: './add-prod-info.component.html',
  styleUrls: ['./add-prod-info.component.scss']

})
export class AddProdInfoComponent implements OnInit {
  
  @Input() Categories:any=[]
  @Input() Types:any=[]

  product_type:any={
    id:"",
    type:""
  }

  size = ["S","M","L","XL","2XL","3XL"]
  Gender = ["Homme","Femme","Enfant"]

  tokenO="*"

  categorie:any
  format_type:any
  data:any
  label:any
  type:any
  color_price:any
  price:any
  prod_print:any
  printing_label:any
  printing_type:any
  format_width:any
  format_height:any
  format_price:any
  format_other:any
  mode_price:any
  mode_size:any
  mode_label:any
  mode_type:any
  user:any
  prod_category_id:any
  prod_category_label:any

  
  Ar_type:any=[]
  print_table:any=[]
  sdm:any=[]
  koba:any=[]
  

  spinn=false;
  heigth = false;
  width = false
  format_cost = false;
  other = false;
  
  constructor(private http:ApiService,private token:TokenStorageServiceService, private myevent: EventparserService) { }

  ngOnInit(): void {
    this.user = this.token.getUser()
    console.log(this.Categories)
    console.log(this.Types)

    for(let i of this.Types){
      if((i.type_category_id == 19 || i.type_category_id == 20) || (i.type_category_id == 19 && i.type_category_id == 20)){
        this.sdm.push(i)
      }
      

      if((i.type_category_id == 16 || i.type_category_id == 17 || i.type_category_id == 18) || (i.type_category_id == 16 && i.type_category_id == 17 && i.type_category_id == 18)){
        this.print_table.push(i)
      }

      if(i.type_category_id == 16 ) {
        this.koba.push(i)
      }
      

    }
  }


  Add_category(){
    var data:any={
      label:this.categorie,
      user:this.user.user_id
    }  
    this.http.Add_category(data).subscribe(res=>{
      var data:any=res
      this.Categories=data.data
      if(data.status==true){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Catégories bien enregistré',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: "Une erreur s'est produite",
          showConfirmButton: false,
          timer: 1500
        })
      }
    }, err=>{

      location.reload()
    })
  }


  Onchange(event:any){

    //this.label=event.target.value
    var itm = parseInt(event.target.value)
    console.log(itm)
    for(let item of this.Categories){
      if(item.category_id == itm){
        this.prod_category_label = item.category_label
        this.myevent.iscategory.next(this.prod_category_label)

        var zola:any=[]

        for(let i of this.Types){
          if(i.type_category_id == itm){
            zola.push(i)
          }

        }

        this.Ar_type = zola
        console.log(this.Ar_type)
      }
    }

  }

  Get_Type(event:any){
    var item = event.target.value
    if(item.indexOf('*') != -1){
      this.product_type.id = item.substr(0,item.indexOf('*'))
      this.product_type.type = item.substr(item.indexOf('*'))

      if(this.product_type.id.indexOf('*')!=-1){
        this.product_type.id = this.product_type.id.substr(0,this.product_type.id.indexOf('*')-1)
      }

      if(this.product_type.type.indexOf('*')!=-1){
        this.product_type.type = this.product_type.type.substr(this.product_type.type.indexOf('*')+1)
      }

      this.Ar_type = this.Types
      console.log(this.Ar_type)
      

    }
    
  }

  onchange2(event:any){
    this.mode_type = event.target.value
    
  }

  onchange3(event:any){
    this.mode_label = event.target.value
    
  }

  onchange4(event:any){
    this.mode_size = event.target.value
    
  }

  TypeProd_printing(event:any){
    this.prod_print = event.target.value
    console.log(this.prod_print)

  }

  TypeProd_format(event:any){
    var data = event.target.value
    for(let item of this.sdm){
      if(item.productype_id == data ){
        if(item.productype_label ==  "sac" || item.productype_label == "sachet"){
          this.heigth = true
          this.width = true 
          this.format_cost = true
          this.other = false
        }else{
          this.other = true
          this.format_cost = true
          this.heigth = false
          this.width = false 
          
        }
      }
      
    }
      

  }

  add_type(){
    let data:any={
      label: this.product_type.type,
      category:this.prod_category_label
    }
    console.log(data)


    this.http.Add_Type(data).subscribe(res=>{
      var data:any=res
      console.log(data)
      if(data.status==true){
        this.Types=data.data
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Enregistré',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: "Une erreur s'est produite",
          showConfirmButton: false,
          timer: 1500
        })
      }
    },err=>{

    })
  }

  ad_printing_type(){
    var data:any={
      label:this.printing_label,
      price:this.price,
      color:this.color_price,
      type: this.prod_print
    }

    console.log(data)

    this.http.Add_printing_type(data).subscribe(res=>{
      var data:any=res
      if(data.status==true){
        Swal.fire({

          icon: 'success',
          title: 'Enregistré',
          showConfirmButton: false,
          
        })
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: "Une erreur s'est produite",
          showConfirmButton: false,
          timer: 1500
        })
      }
    },err=>{
   
    })
  }

  ad_format(){
    var data:any={
      width:this.format_width,
      height:this.format_height,
      type:this.format_type,
      price:this.format_price,
      other:this.format_other || null

    }
    console.log(data)

    this.http.Add_format(data).subscribe(res=>{

      var data:any=res
      if(data.status==true){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Enregistré',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: "Une erreur s'est produite",
          showConfirmButton: false,
          timer: 1500
        })
      }
    },err=>{
      location.reload()
    })
  }

  add_mode(){
    var data:any={
      label:this.mode_label,
      type:this.mode_type,
      price:this.mode_price,
      size:this.mode_size

    }
    console.log(data)

    this.http.Add_mode(data).subscribe(res=>{

      var data:any=res
      if(data.status==true){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Enregistré',
          showConfirmButton: false,
          timer: 1500
        })

      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: "Une erreur s'est produite",
          showConfirmButton: false,
          timer: 1500
        })
      }
    },err=>{
      location.reload()

    })
  }


  setprinting(){

  }
}
