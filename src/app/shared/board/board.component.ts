import { EventparserService } from 'src/app/core';
import { Component, OnInit, Input,  OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  @Input() orders:any=[];
  @Input() Data:any=[];
  @Input() Type:any
  @Input() P_type:any
  @Input() Format:any
  @Input() Mode:any

  spiner = false
  order = false
  delivery = false
  update = false
  constructor(private myevent: EventparserService) { }
  

  ngOnInit(): void {
    console.log(this.orders)
    console.log(this.Data)
    


    this.myevent.new_orders.subscribe(resp=>{
      this.order = resp
      this.delivery = false
      this.update = false
    })

    this.myevent.add_livry.subscribe(resp=>{
      this.delivery = resp
      this.order = false
      this.update = false
      
    })

    this.myevent.update_pro.subscribe(resp=>{
      this.update = resp
      this.delivery = false
      this.order = false
    })


  }



}
