import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  BaseUrl:any= environment.baseUrlDev
  constructor(private http:HttpClient) { }

  getOrders(edirot:boolean=true):Observable<any>{
    if(edirot){
      return this.http.get<any>(environment.prodApiOrder+"editor-orders")

    }else{
      return this.http.get<any>(environment.prodApiOrder+"crea-orders")

    }
  }


  
  setDeliveryAdr(data:any):Observable<any>{
   return this.http.patch(this.BaseUrl+"delivery",data)
   }


   getdeliveryadr():Observable<any>{
     return this.http.get<any>(this.BaseUrl+"delivery");
  }

  getprod_print():Observable<any>{

   return this.http.get<any>(environment.testgil+'products/printing')

  }

  setprinting(data:any):Observable<any>{
    return this.http.patch<any>(this.BaseUrl+'products/printing',data)

  }
  

   
  saveDeliveryAdr(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"delivery",data)
  }

  getShape(id:any):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"editor/shapes/"+id)
  }

  getPdf(data:any):Observable<any>{
    return this.http.patch<any>(environment.prodApiOrder+"pdf",data)

  }

  getOrdersByStatus(status:string):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"users/orders/"+status)
  }

  getprod_format(){
    return this.http.get<any>(environment.testgil+"products/format")

  }

  getprod_mode(){
    return this.http.get<any>(environment.testgil+"products/mode")

  }

  inviteUser(data:any){
    return this.http.post<any>(this.BaseUrl+"admin/invite",data)

  }

  setStatus(id:string|any,status:string):Observable<any>{
    return this.http.patch<any>(this.BaseUrl+"users/orders/"+id,{status:status})
  }

  saveDesigns(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"products/designs",data)

  }

  upDateProduct(data:any):Observable<any>{
    return this.http.patch<any>(this.BaseUrl+"products",data)

  }

  upDateUser(data:any){
    return this.http.patch<any>(this.BaseUrl+"admin",data)
  }


  saveUser(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"admin",data)
  }


  getUser(){
    return this.http.get<any>(this.BaseUrl+"admin")
  }

  postUserRole(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"admin/roles",data)
  }


  getProducts(name:any):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"products/"+name)
  }


  saveProduct(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"products",data)
  }

  getProductType():Observable<any>{
    return this.http.get<any>(environment.testgil+"products/type")
  }

  getType=(category:string):Observable<any>=>{
    return this.http.get<any>(this.BaseUrl+"products/type/"+category)
  }

  getAllCategories():Observable<any>{
    return this.http.get<any>(this.BaseUrl+"products/category")
  }

  saveTheme(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"editor/themes",data)
  }

  saveItem(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"editor/items",data)
  }


  geTItem(id:any):Observable<any>{
    return  this.http.get<any>(this.BaseUrl+"editor/items/"+id)
  }

  getType_theme(id:any):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"editor/themes/"+id)
  }

  saveFile(formdata:any):Observable<any>{

    return this.http.post<any>(this.BaseUrl+"upload",formdata)
  }
  //categories

  Add_category(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"products/category", data)
  }
//productType

Add_Type(data:any):Observable<any>{
  return this.http.post<any>(environment.testgil+"products/type", data)
}
//printing
Add_printing_type(data:any):Observable<any>{
  return this.http.post<any>(environment.testgil+"products/printing", data)
}
//format

Add_format(data:any):Observable<any>{
  return this.http.post<any>(environment.testgil+"products/format", data)
}
Add_mode(data:any):Observable<any>{
  return this.http.post<any>(environment.testgil+"products/mode", data)
}

getDesigns(){
  return this.http.get<any>(this.BaseUrl+"products/designs")

}

setDesign(id:any){
  return this.http.patch<any>(this.BaseUrl+"products/designs",{design:id})
}

setProd_format(id:any, price:number):Observable<any>{
  return this.http.patch<any>(environment.testgil+"products/format/"+id,{price: price} )
}

setProd_printing(id:any, price:number, color_price:number, discount:number):Observable<any>{
  return this.http.patch<any>(environment.testgil+"products/printing/"+id,{printing_price: price, color_price: color_price, discount: discount} )
}

setProd_mode(id:any, price:number):Observable<any>{
  return this.http.patch<any>(environment.testgil+"products/mode/"+id,{price: price} )
}

setProd_type(id:any, disount:number):Observable<any>{
  return this.http.patch<any>(environment.testgil+"products/type/"+id,{product_type_discount: disount} )
}






}
